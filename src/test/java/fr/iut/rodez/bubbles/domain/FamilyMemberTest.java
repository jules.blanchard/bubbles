package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FamilyMemberTest {

    @Test
    void shouldBeAbleToCreateFamilyMember() {
        // Given
        UUID id = UUID.randomUUID();
        String name = "John";
        Identity identity = new Identity(id, name, null);
        Position position = new Position(0, 0);

        // When
        FamilyMember john = new FamilyMember(identity, position);

        // Then
        assertSame(identity, john.identity);
    }

    @Test
    void shouldBeAbleToCreateParentalRelationship() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));
        FamilyMember bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", null), new Position(20, 20));

        // When
        john.parentOf(bob);

        // Then
        Set<Relation> johnRelations = john.relations();
        assertSame(1, johnRelations.size());
        Relation johnRelation = johnRelations.iterator()
                                             .next();
        assertSame(bob, johnRelation.related());
        assertInstanceOf(ChildRelation.class, johnRelation);

        Set<Relation> bobRelations = bob.relations();
        assertSame(1, bobRelations.size());
        Relation bobRelation = bobRelations.iterator()
                                           .next();
        assertSame(john, bobRelation.related());
        assertInstanceOf(ParentRelation.class, bobRelation);
    }

    @Test
    void whenHasAChildShouldHaveThatChildInTheirChildren() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));
        FamilyMember bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", null), new Position(20, 20));

        // When
        john.parentOf(bob);

        // Then
        Set<FamilyMember> children = john.children();

        assertSame(1, children.size());
        assertTrue(children.contains(bob));
    }

    @Test
    void whenDoesNotHaveAChildShouldHaveAnEmptyChildren() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));

        // Then
        Set<FamilyMember> children = john.children();

        assertSame(0, children.size());
    }

    @Test
    void whenHasChildrenAndParentsShouldHaveThatChildrenAndParentsInTheirLovedOnes() {
        // Given
        FamilyMember sam = new FamilyMember(new Identity(UUID.randomUUID(), "Sam", null), new Position(0, 0));
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(10, 10));
        FamilyMember bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", null), new Position(20, 20));
        FamilyMember alice = new FamilyMember(new Identity(UUID.randomUUID(), "Alice", null), new Position(30, 30));

        // When
        sam.parentOf(john);
        john.parentOf(bob);
        john.parentOf(alice);

        // Then
        Set<FamilyMember> johnLovedOnes = john.lovedOnes();

        assertSame(3, johnLovedOnes.size());
        assertTrue(johnLovedOnes.contains(bob));
        assertTrue(johnLovedOnes.contains(alice));
        assertTrue(johnLovedOnes.contains(sam));

        Set<FamilyMember> samLovedOnes = sam.lovedOnes();

        assertSame(1, samLovedOnes.size());
        assertTrue(samLovedOnes.contains(john));

        Set<FamilyMember> bobLovedOnes = bob.lovedOnes();

        assertSame(1, bobLovedOnes.size());
        assertTrue(bobLovedOnes.contains(john));
    }

    @Test
    void whenFamilyHasCircleRelationshipShouldNotHaveInfiniteRelatives() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));
        FamilyMember bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", null), new Position(10, 10));
        FamilyMember alice = new FamilyMember(new Identity(UUID.randomUUID(), "Alice", null), new Position(20, 20));

        // When
        john.parentOf(bob);
        bob.parentOf(alice);
        alice.parentOf(john); // does not have any sense but it is to test

        // Then
        Set<FamilyMember> johnRelatives = john.relatives(3);
        assertSame(2, johnRelatives.size());
        assertTrue(johnRelatives.contains(bob));
        assertTrue(johnRelatives.contains(alice));

        Set<FamilyMember> bobRelatives = bob.relatives(3);
        assertSame(2, bobRelatives.size());
        assertTrue(bobRelatives.contains(john));
        assertTrue(bobRelatives.contains(alice));

        Set<FamilyMember> aliceRelatives = alice.relatives(3);
        assertSame(2, aliceRelatives.size());
        assertTrue(aliceRelatives.contains(john));
        assertTrue(aliceRelatives.contains(bob));
    }

    @Test
    void shouldThrowExceptionWhenMaxDegreeForRelativesIsNotStrictlyPositive() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));

        // Then
        var exception = assertThrows(IllegalArgumentException.class, () -> john.relatives(0));
        assertEquals("Max degree must be at least 1.", exception.getMessage());
    }

    @Test
    void shouldBeAbleToProvideRelations() {
        // Given
        FamilyMember john = new FamilyMember(new Identity(UUID.randomUUID(), "John", null), new Position(0, 0));
        FamilyMember bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", null), new Position(10, 10));
        FamilyMember alice = new FamilyMember(new Identity(UUID.randomUUID(), "Alice", null), new Position(20, 20));

        // When
        john.parentOf(bob);
        john.parentOf(alice);

        // Then
        var johnRelations = john.relations();
        assertSame(2, johnRelations.size());
        var thoseRelatedToJohn = johnRelations.stream()
                                              .map(Relation::related)
                                              .toList();
        assertTrue(thoseRelatedToJohn.containsAll(Set.of(bob, alice)));

        var bobRelations = bob.relations();
        assertSame(1, bobRelations.size());
        var thoseRelatedToBob = bobRelations.stream()
                                            .map(Relation::related)
                                            .toList();
        assertTrue(thoseRelatedToBob.contains(john));

        var aliceRelations = alice.relations();
        assertSame(1, aliceRelations.size());
        var thoseRelatedToAlice = aliceRelations.stream()
                                                .map(Relation::related)
                                                .toList();
        assertTrue(thoseRelatedToAlice.contains(john));
    }
}