package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.Position;

public interface Mobile {

    void moveTo(Position position);
}
