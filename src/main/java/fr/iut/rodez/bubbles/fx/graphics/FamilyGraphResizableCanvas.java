package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.EDGE_MANIPULATION;

public class FamilyGraphResizableCanvas extends ResizableCanvas {

    private final GraphicalFamily family;

    private final List<GraphicalFamilyMember> selectedMembers;
    private final GraphicsContext context;

    private EventManagementStrategy eventManagementStrategy;

    private Position point;

    public FamilyGraphResizableCanvas(GraphicalFamily family, EventManagementStrategy eventManagementStrategy) {
        Objects.requireNonNull(family, "family must not be null");
        Objects.requireNonNull(eventManagementStrategy, "event strategy must not be null");

        this.family = family;
        this.eventManagementStrategy = eventManagementStrategy;
        this.selectedMembers = new ArrayList<>();
        this.context = getGraphicsContext2D();

        configureCanvasEvents();
    }

    public void setEventManagementStrategy(EventManagementStrategy eventManagementStrategy) {
        this.eventManagementStrategy = eventManagementStrategy;
    }

    public void configureCanvasEvents() {
        setOnMouseDragged(event -> {
            Position position = new Position(event.getX(), event.getY());
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> moveSelectedElementsTo(position);
                case EDGE_MANIPULATION -> definePointByReachableElement(position);
            }
            event.consume();
        });

        setOnMousePressed(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION, EDGE_MANIPULATION ->
                        selectFirstElementPositionedOn(new Position(event.getX(), event.getY()));
            }
            event.consume();
        });

        setOnMouseReleased(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> clearSelection();
                case EDGE_MANIPULATION -> selectedAsParentsOfPotentialOne(new Position(event.getX(), event.getY()));
            }
            event.consume();
        });

        setOnMouseExited(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> clearSelection();
                case EDGE_MANIPULATION -> clearSelectionAndPoint();
            }
            event.consume();
        });
    }

    private void moveSelectedElementsTo(Position position) {
        if (!selectedMembers.isEmpty()) {
            selectedMembers.forEach(element -> element.moveTo(position));
            draw();
        }
    }

    private void selectFirstElementPositionedOn(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(element -> {
            selectedMembers.clear();
            selectedMembers.add(element);
        });
    }

    private void definePointByReachableElement(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresentOrElse(member -> {
            Position memberPosition = member.currentPosition();
            point = new Position(memberPosition.x(), memberPosition.y());
        }, () -> point = position);
        draw();
    }

    private void selectedAsParentsOfPotentialOne(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(member -> selectedMembers.forEach(other -> family.createRelation(other, member)));
        clearSelectionAndPoint();
    }

    private void clearSelection() {
        selectedMembers.clear();
        draw();
    }

    private void clearSelectionAndPoint() {
        selectedMembers.clear();
        point = null;
        draw();
    }

    private boolean isTemporaryLineToDraw() {
        return eventManagementStrategy == EDGE_MANIPULATION && point != null && !selectedMembers.isEmpty();
    }

    private Optional<GraphicalFamilyMember> firstFamilyMemberPositionedOn(Position position) {
        return family.members
                .stream()
                .filter(member -> member.covers(position))
                .findFirst();
    }

    @Override
    protected void draw() {
        context.clearRect(0, 0, getWidth(), getHeight());

        family.relations.forEach(relation -> relation.drawWith(context));

        if (isTemporaryLineToDraw() && !selectedMembers.isEmpty()) {
            var firstElement = selectedMembers.getFirst();
            var position = firstElement.currentPosition();
            context.setLineWidth(4.0);
            context.setStroke(Color.GREY);
            context.strokeLine(point.x(), point.y(), position.x(), position.y());
        }

        family.members.forEach(member -> member.drawWith(context));
    }

    public enum EventManagementStrategy {
        NODE_MANIPULATION, EDGE_MANIPULATION
    }
}
