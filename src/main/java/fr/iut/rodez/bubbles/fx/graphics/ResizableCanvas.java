package fr.iut.rodez.bubbles.fx.graphics;

import javafx.scene.canvas.Canvas;

// https://www.javacodegeeks.com/2014/04/javafx-tip-1-resizable-canvas.html
public abstract class ResizableCanvas extends Canvas {

    public ResizableCanvas() {
        widthProperty().addListener(evt -> draw());
        heightProperty().addListener(evt -> draw());
    }

    @Override
    public final boolean isResizable() {
        return true;
    }

    @Override
    public final double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public final double prefHeight(double width) {
        return getHeight();
    }

    protected abstract void draw();
}
