package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.Relation;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toMap;

public class GraphicalFamily {

    public final ListProperty<GraphicalFamilyMember> members;
    public final ListProperty<GraphicalRelation> relations;

    public GraphicalFamily(Family family) {

        Map<UUID, GraphicalFamilyMember> membersById = family.members()
                                                             .stream()
                                                             .collect(toMap(f -> f.identity.id(), GraphicalFamilyMember::new));

        this.members = new SimpleListProperty<>(FXCollections.observableArrayList(membersById.values()));
        this.relations = new SimpleListProperty<>(FXCollections.observableArrayList());

        family.members()
              .forEach(member -> {
                  Set<Relation> relations = member.relations();
                  relations.forEach(relation -> {
                      GraphicalFamilyMember source = membersById.get(member.identity.id());
                      GraphicalFamilyMember target = membersById.get(relation.related().identity.id());
                      createRelation(source, target);
                  });
              });
    }

    public void createRelation(GraphicalFamilyMember source, GraphicalFamilyMember target) {
        relations.add(new GraphicalRelation(source, target));
        source.parentOf(target);
    }
}
